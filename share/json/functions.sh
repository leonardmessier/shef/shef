#!/bin/bash

_shef_json_edit_property() {
    local property=$1
    local content=$2
    local target=$3

    local json_query=".$property = $content"

    cat $target | jq -r "$json_query"
}

_shef_json_remove_property() {
    local property=$1
    local target=$2

    local json_query="del(.$property)"
    cat $target | jq -r "$json_query"
}

_shef_json_validate_file() {
    local filename=$1

    if ! jq -r '.' $filename > /dev/null 2>&1; then
        return 1
    else
        return 0
    fi
}
