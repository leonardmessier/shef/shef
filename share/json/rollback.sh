#!/bin/bash

_shef_json_rollback() {
    local property=$2
    local target=$3

    # shellcheck disable=SC2086
    new_content=$(_shef_json_remove_property $property $target)

    # shellcheck disable=SC2086
    if echo -e "$new_content" | sponge $target; then
        shef_success "json" "Content removed from target"
    else
        shef_error "json" "Content could not be removed from target"
    fi
}
