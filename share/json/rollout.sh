#!/bin/bash

_shef_json_rollout() {
    local action=$1
    local property=$2
    local from_file=$3
    local target=$4

    local content
    local new_content
    if [ "$action" == "edit" ]; then
        content=$(cat "$from_file")
        # shellcheck disable=SC2086
        new_content=$(_shef_json_edit_property $property "$content" $target)

        # shellcheck disable=SC2086
        if echo -e "$new_content" | sponge $target; then
            shef_success "json" "New content written to target"
        else
            shef_error "json" "New content could not be written to target"
        fi
    fi

}
