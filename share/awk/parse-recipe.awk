# One line instructions
# =====================

# Handles lines with just a command, without a block .i.e.,
# - lines not ending with `; do` (start of multiline block)
# - lines with only `done` (end of multiline block)
# - lines with 4 spaces indentation (body of a multiline block, the arguments)
# - non empty lines

# Recipe file:
# brew "git-flow"
#
# Result:
# _shef_queue:add RECIPE_NAME 'brew "git-flow"' || return "$?"

($0 !~ /; do$/ && $0 !~ /^done$/ && $0 !~ /^(\s){4}/ && $0 !~ /^#/ && $0 != "") {
    printf "_shef_queue:add " RECIPE_NAME " \"$(cat <<-EOF\n" $0 "\nEOF\n)\"\n"
}

# Multiline instructions
# ======================

# Let's take the example of a git block. Each `Result:` section will hold the
# concatenated result of the parsing

# -- BLOCK FIRST LINE --

# Handles lines ending with `; do`

# Recipe file:
# git "Downloading repository"; do

# Result:
# _shef_queue:add RECIPE_NAME "$(cat <<-EOF
# git "Downloading repository"

($0 ~ /; do$/ && $0 !~ /^#/) {
    sub(/; do$/, "", $0)
    sub(/'/, "\\'", $0)
    RESOURCE_NAME=$1
    printf "_shef_queue:add " RECIPE_NAME " \"$(cat <<-EOF\n" $0
}

# -- BLOCK BODY --

# Handles lines starting with 4 spaces indentation
# New lines are not preserved and arguments and their possible values are
# inlined as part of the heredoc string initialized above

# Recipe file:
#     repository "https://github.com/CaptainQuirk/.ranger-config"

# Result:
# _shef_queue:add RECIPE_NAME "$(cat <<-EOF
# git "Downloading repository" --repository "https://github.com/CaptainQuirk/.ranger-config"

($0 ~ /^(\s){4}/) {
    if ($2 == "true") {
        printf " --" $1
    } else {
        STR=""
        for (i=2; i<=NF; i++)
          STR=STR FS $i
        STR=STR

        ENCODED=""
        RESULT=""
        if (RESOURCE_NAME == "code" && $1 == "code") {
            CMD="echo '" STR "' | base64 -w 0"
            while (( CMD | getline RESULT ) > 0 ) {
                ENCODED=ENCODED RESULT
            }
            close(CMD)
            STR=ENCODED
        } else {
            gsub(/%/, "%%", STR)
        }
        RES=" --" $1 FS STR
        printf RES
    }
}

# -- BLOCK END --

# Handle lines containing `done`
# Adds a ` || return $?` and an `EOF` marker on a new line to terminate the
# heredoc string

# Recipe file:
# done

# Result:
# _shef_queue:add RECIPE_NAME "$(cat <<-EOF
# git "Downloading repository" --repository "https://github.com/CaptainQuirk/.ranger-config" || return $?
# EOF

# Last block line is used to add a return
# statement with the _shef_queue:add command exit code
($0 ~ /^done$/) {
    printf "\nEOF\n)\"\n"
}
