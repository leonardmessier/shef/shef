if [[ ! -o interactive ]]; then
    return
fi

compctl -K _shef shef

_shef() {
  local word words completions
  read -cA words
  word="${words[2]}"

  if [ "${#words}" -eq 2 ]; then
    completions="$(shef commands)"
  else
    completions="$(shef completions "${word}")"
  fi

  reply=("${(ps:\n:)completions}")
}
