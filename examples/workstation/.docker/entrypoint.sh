#!/bin/bash

if [ "$1" == "bash" ]; then
    exec /bin/bash
    exit $?
fi

if ! shef install --very-verbose --ignore-system-updates; then
    OUTCOME=$?
    cat /tmp/shef.log
    exit $OUTCOME
fi

exit 0
