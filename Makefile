COMMANDS := $(MAKEFILE_LIST)

include .env
export

# Check if container-structure-test is present
CONTAINER_TEST := $(shell command -v container-structure-test 2>/dev/null)

all: lint test-unit build test-example test-acceptance

build:
	@docker-compose build --no-cache shef

lint:
	@docker-compose -f .docker/docker-compose.yml run --rm shellcheck libexec/* share/functions share/**/functions install.sh

test: test-acceptance test-unit test-example

test-unit:
	@docker-compose -f .docker/docker-compose.test.yml run --rm bats -r /code/tests/unit

test-acceptance:
ifndef CONTAINER_TEST
	@echo "container-structure-test is not available. Follow instructions here : https://github.com/GoogleContainerTools/container-structure-test"
	exit 1
endif

ifndef SHEF_VERSION
	@echo "SHEF_VERSION variable is not set"
	exit 1
endif

ifndef SHEF_BASE_SYSTEM_VERSION
	@echo "SHEF_BASE_SYSTEM_VERSION variable is not set"
	exit 1
endif

	@container-structure-test test \
		--image registry.gitlab.com/leonardmessier/shef/shef:$(SHEF_VERSION)-$(SHEF_BASE_SYSTEM_VERSION) \
		--config tests/acceptance/cst.yml \
		--config tests/acceptance/shef-cron.yml \
		--config tests/acceptance/shef-link.yml \
		--config tests/acceptance/shef-init.yml \
		--config tests/acceptance/shef-git.yml \
	@echo "\033[32m✓\033[0m Acceptance tests passed"

test-example:
	@echo "Running shef on example set up"
	@cd examples/workstation; docker-compose -f .docker/docker-compose.yml run --rm workstation

.DEFAULT: all
.PHONY: build lint test test-unit test-acceptance test-example





