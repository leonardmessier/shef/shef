#!/bin/bash

declare -ir ERROR_NOT_ROOT=1
declare -ir ERROR_MISSING_DEP=2
declare -ir ERROR_DOWNLOAD_ARCHIVE=3
declare -ir ERROR_EXTRACT_ARCHIVE=4
declare -ir ERROR_BINARY_INSTALL=5
declare -ir ERROR_BINARY_EXECUTABLE=5
declare -ir ERROR_BINARY_SYMLINK=6
declare -ir ERROR_SHARED_APPLICATIONS_DIR=7
declare -ir ERROR_DESKTOP_FILE_COMPILATION=8
declare -ir ERROR_DESKTOP_FILE_EXECUTABLE=9
declare -ir ERROR_SHARED_ICONS_DIR=10
declare -ir ERROR_DESKTOP_ICON_COPY=11
declare -ir ERROR_AUTOSTART_DIR=12
declare -ir ERROR_DESKTOP_FILE_SYMLINK=13
declare -ir ERROR_WRAPPER_COPY=14
declare -ir ERROR_WRAPPER_EXECUTABLE=15
declare -ir ERROR_HOMEBREW_DEPENDENCIES_INSTALL=16
declare -ir ERROR_HOMEBREW_INSTALL=17
declare -ir ERROR_SHEF_HOMEBREW_DEPENDENCIES_INSTALL=18
declare -ir ERROR_SHEF_APT_DEPENDENCIES_INSTALL=19
declare -ir ERROR_ESH_DOWNLOAD=20
declare -ir ERROR_ESH_EXECUTABLE=21
declare -ir ERROR_ESH_MOVE=22
declare -ir ERROR_PARSE_INI_DOWNLOAD=23
declare -ir ERROR_PARSE_INI_EXECUTABLE=24
declare -ir ERROR_PARSE_INI_MOVE=25
declare -ir ERROR_INSTALL_DIR_CREATION=26
declare -ir ERROR_BINARY_PATH_CREATION=27

set -e

if ! OPTS=$(getopt -o vhns: --long archive-url:,archive-top-folder:,ignore-dependencies, -n 'parse-options' -- "$@"); then
    shef_error "git" "Failed parsing options." >&2
    exit 1
fi

eval set -- "$OPTS"

IGNORE_DEPENDENCIES=false
ARCHIVE_URL="https://gitlab.com/leonardmessier/shef/shef/-/archive/master/shef-master.tar.gz"
ARCHIVE_TOP_FOLDER="shef-master"

while true; do
    case "$1" in
        --ignore-dependencies ) IGNORE_DEPENDENCIES=true; shift; shift ;;
        --archive-url ) ARCHIVE_URL=$2; shift; shift ;;
        --archive-top-folder ) ARCHIVE_TOP_FOLDER=$2; shift; shift ;;
        * ) break;;
    esac
done

_is_root() {
    ID="$(id -u)"
    if [ "$ID" == 0 ]; then
        return 0
    else
        return 1
    fi
}

_is_installed() {
    local DEP=$1
    if command -v "$DEP" > /dev/null 2>&1; then
      return 0
    else
      return 1
    fi
}

info() {
  local MSG=$1

  echo -e "$MSG"
}

error() {
  local MSG=$1

  echo -e "$MSG"
}

success() {
  local MSG=$1

  echo -e "$MSG"
}

configure() {
    local DEPS=("brew" "shml" "wget" "git" "go" "jq" "gawk" "python3" "pip3" "esh" "parse-ini" )

    if _is_root; then
        echo "This should not be run as root"
        return $ERROR_NOT_ROOT
    fi

    if [ "$IGNORE_DEPENDENCIES" == true ]; then
        return 0
    fi

    for DEP in "${DEPS[@]}"
    do
        if ! _is_installed "$DEP"; then
            echo "$DEP is not installed. Exiting"
            return $ERROR_MISSING_DEP
        fi
    done
}

download() {
    local FILE_PATH=$2
    local ARCHIVE_URL=$1

    if ! curl --silent -o "$FILE_PATH" "$ARCHIVE_URL"; then
        error "Archive could not be dowloaded. Try to run \`curl -o $FILE_PATH $ARCHIVE_URL\`"
        return $ERROR_DOWNLOAD_ARCHIVE
    fi

    success "Archive $ARCHIVE_URL dowloaded into $FILE_PATH"
}

extract() {
    ARCHIVE_PATH=$1
    DESTINATION=$2

    if ! tar xzf "$ARCHIVE_PATH" -C "$DESTINATION"; then
        error "Archive could not be extracted. Try to run \`tar xzf $ARCHIVE_PATH -C $DESTINATION\`"
        return $ERROR_EXTRACT_ARCHIVE
    fi

    success "Archive $ARCHIVE_PATH extracted into $DESTINATION"
}

install_binary() {
    local SOURCE=$1
    local INSTALL_DESTINATION=$HOME/.local/opt/
    local BINARY_PATH=$HOME/.local/bin

    if ! mkdir -p "$INSTALL_DESTINATION"; then
      error "Destination folder $INSTALL_DESTINATION could not be created"
      return $ERROR_INSTALL_DIR_CREATION
    else
      info "Destination folder $INSTALL_DESTINATION created"
    fi

    if ! mkdir -p "$BINARY_PATH"; then
      error "Binary folder $BINARY_PATH could not be created"
      return $ERROR_BINARY_PATH_CREATION
    else
      info "Binary path $BINARY_PATH created"
    fi

    if ! cp -r "$SOURCE" "$INSTALL_DESTINATION/shef"; then
      error "Source $SOURCE could not be copied to $INSTALL_DESTINATION"
      return $ERROR_BINARY_INSTALL
    else
      info "Source $SOURCE copied to $INSTALL_DESTINATION"
    fi
    success "Source $SOURCE moved to $DESTINATION"

    if ! chmod +x "$INSTALL_DESTINATION/shef/bin/shef"; then
        error "Error making the binary $INSTALL_DESTINATION/shef/bin/shef executable"
        return $ERROR_BINARY_EXECUTABLE
    fi
    success  "$INSTALL_DESTINATION/shef/bin/shef binary was made executable"

    info "Symlinking $INSTALL_DESTINATION/shef/bin/shef to $BINARY_PATH"
    if ! ln -f -s "$INSTALL_DESTINATION/shef/bin/shef" "$BINARY_PATH"; then
        error "$INSTALL_DESTINATION/shef/bin/shef could not be symlinked to $BINARY_PATH"
        return $ERROR_BINARY_SYMLINK
    fi
    success "$INSTALL_DESTINATION/shef/bin/shef was symlinked to $BINARY_PATH"
}

install_desktop_file() {
    local SOURCE=$1

    if ! mkdir -p "$HOME/.local/share/applications/"; then
        error "Could not create $HOME/.local/share/applications directory"
        return $ERROR_SHARED_APPLICATIONS_DIR
    fi
    success "$HOME/.local/share/applications directory created"

    if ! esh -o "$HOME/.local/share/applications/shef-install.desktop" \
        "$SOURCE/share/desktop/shef-install.desktop.esh" \
        "HOME=$HOME"; then
        error "Could not compile $SOURCE/share/desktop/shef-install.desktop.esh with esh"
        return $ERROR_DESKTOP_FILE_COMPILATION
    fi
    success "$SOURCE/share/desktop/shef-install.desktop.esh compiled with esh to $HOME/.local/share/applications/shef-install.desktop"

    if ! chmod +x "$HOME/.local/share/applications/shef-install.desktop"; then
        error "Could not make $HOME/.local/share/applications/shef-install.desktop executable"
        return $ERROR_DESKTOP_FILE_EXECUTABLE
    fi
    success "$HOME/.local/share/applications/shef-install.desktop is now executable"

    if ! mkdir -p "$HOME/.local/share/icons"; then
        error "Could not create $HOME/.local/share/icons directory"
        return $ERROR_SHARED_ICONS_DIR
    fi
    success "$HOME/.local/share/icons directory created"

    if ! cp "$SOURCE/share/desktop/shef-install.png" \
        "$HOME/.local/share/icons/shef-install.png"; then
        error "Could not copy $SOURCE/share/desktop/shef-install.png to $HOME/.local/share/icons/shef-install.png"
        return $ERROR_DESKTOP_ICON_COPY
    fi
    success "$SOURCE/share/desktop/shef-install.png was copied to $HOME/.local/share/icons/shef-install.png"

    if ! mkdir -p "$HOME/.config/autostart"; then
        error "Could not create $HOME/.config/autostart directory"
        return $ERROR_AUTOSTART_DIR
    fi
    success "Could not create $HOME/.config/autostart directory"

    if ! ln -f -s "$HOME/.local/share/applications/shef-install.desktop" \
       "$HOME/.config/autostart/shef-install.desktop"; then
        error "Could not symlink $HOME/.local/share/applications/shef-install.desktop to $HOME/.config/autostart/shef-install.desktop directory"
        return $ERROR_DESKTOP_FILE_SYMLINK
    fi
    success "$HOME/.local/share/applications/shef-install.desktop symlinked to $HOME/.config/autostart/shef-install.desktop directory"

    local INSTALL_WRAPPER_PATH=/usr/local/bin/shef-install-wrapper
    if ! sudo cp "$SOURCE/share/desktop/shef-install-wrapper" \
        "$INSTALL_WRAPPER_PATH"; then
        error "$SOURCE/share/desktop/shef-install-wrapper could not be moved to $INSTALL_WRAPPER_PATH"
        return $ERROR_WRAPPER_COPY
    fi

    if ! sudo chmod +x "$INSTALL_WRAPPER_PATH"; then
        error "Could not make $INSTALL_WRAPPER_PATH executable"
        return $ERROR_WRAPPER_EXECUTABLE
    fi
    success "$INSTALL_WRAPPER_PATH is now executable"
}

install_homebrew() {
    if command -v brew >/dev/null 2>&1; then
      info "Homebrew is already installed"
      return 0
    fi

    if ! sudo apt-get install build-essential curl file git; then
        error "Could not install homebrew dependencies with apt"
        return $ERROR_HOMEBREW_DEPENDENCIES_INSTALL
    fi
    success "Homebrew dependencies were installed"

    if ! /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"; then
        error "Homebrew was installed"
        return $ERROR_HOMEBREW_INSTALL
    fi

    test -d ~/.linuxbrew && eval "$(~/.linuxbrew/bin/brew shellenv)"
    test -d /home/linuxbrew/.linuxbrew && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
    test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
    echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile
}

install_shef_dependencies() {
    local BREW_DEPS=("shml" "git" "go" "jq" "gawk" "python3")
    for BREW_DEP in "${BREW_DEPS[@]}"
    do
        if _is_installed "$BREW_DEP"; then
            info "$BREW_DEP is already installed"
        else
          if ! brew install "$BREW_DEP"; then
            error "Shef homebrew dependency $BREW_DEP could not be installed"
            return $ERROR_SHEF_HOMEBREW_DEPENDENCIES_INSTALL
          fi
        fi

    done
    success "Shef homebrew dependencies are installed"

    local APT_DEPS=("wget" "moreutils")
    for APT_DEP in "${APT_DEPS[@]}"
    do
        if _is_installed "$APT_DEP"; then
            info "$APT_DEP is already installed"
        else
          if ! sudo apt-get install "$APT_DEP"; then
            error "Shef apt dependency $APT_DEP could not be installed"
            return $ERROR_SHEF_APT_DEPENDENCIES_INSTALL
          fi
        fi
    done
    success "Shef apt dependencies are installed"

    if _is_installed esh; then
      info "esh is already installed"
    else
      ESH_URL=https://raw.githubusercontent.com/jirutka/esh/master/esh
      if ! wget $ESH_URL; then
          error "$ESH_URL could not be downloaded"
          return $ERROR_ESH_DOWNLOAD
      fi

      if ! chmod +x "$HOME/esh"; then
          error "Could not make $HOME/esh executable"
          return $ERROR_ESH_EXECUTABLE
      fi
      local ESH_INSTALL_PATH=/usr/local/bin/esh
      if ! sudo mv "$HOME/esh" "$ESH_INSTALL_PATH"; then
          error "Could not move $HOME/esh to $ESH_INSTALL_PATH"
          return $ERROR_ESH_MOVE
      fi

      success "Esh installed"
    fi

    if _is_installed parse-ini; then
      info "parse-ini is installed"
    else
      PARSE_INI_URL=https://gitlab.com/leonardmessier/utils/parse-ini/-/raw/master/bin/parse-ini
      if ! wget $PARSE_INI_URL; then
          error "$PARSE_INI_URL could not be downloaded"
          return $ERROR_PARSE_INI_DOWNLOAD
      fi

      if ! chmod +x "$HOME/parse-ini"; then
          error "Could not make $HOME/parse-ini executable"
          return $ERROR_PARSE_INI_EXECUTABLE
      fi

      local PARSE_INI_INSTALL_PATH=/usr/local/bin/parse-ini
      if ! sudo mv "$HOME/parse-ini" "$PARSE_INI_INSTALL_PATH"; then
          error "Could not move $HOME/parse-ini to $PARSE_INI_INSTALL_PATH"
          return $ERROR_PARSE_INI_MOVE
      fi
      success "parse-ini installed"
    fi
}

install_homebrew
install_shef_dependencies
download "$ARCHIVE_URL" /tmp/shef-master.tar.gz
extract /tmp/shef-master.tar.gz /tmp
install_binary "/tmp/$ARCHIVE_TOP_FOLDER"
install_desktop_file "/tmp/$ARCHIVE_TOP_FOLDER"

export PATH=$PATH:$HOME/.local/bin

shef init
