load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_SHEF_ROOT/tests/unit/helpers/assert"

load "$_SHEF_ROOT/share/awk/functions"

setup() {
    # shellcheck disable=SC1091
    . shellmock
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "_shef_build_command produces a recipe file" {
    # GIVEN
    RECIPE_NAME=ranger
    RECIPE_CONTENT="$(cat <<-EOF
brew ranger

git "Install configuration"; do
    repository "https://github.com/CaptainQuirk/.ranger-config"
    destination "/root/.config/.ranger-config"
    enable-submodules true
done
EOF
    )"
    mkdir -p "$BATS_TMPDIR/awk/"
    echo "$RECIPE_CONTENT" > "$BATS_TMPDIR/awk/install.recipe"

    # WHEN
    run _shef_build_command "$RECIPE_NAME" "$BATS_TMPDIR/awk/install.recipe"

    # THEN
    assert_success
    assert_output "$(cat <<-"EOL"
_shef_queue:add ranger "$(cat <<-EOF
brew ranger
EOF
)"
_shef_queue:add ranger "$(cat <<-EOF
git "Install configuration" --repository  "https://github.com/CaptainQuirk/.ranger-config" --destination  "/root/.config/.ranger-config" --enable-submodules
EOF
)"
EOL
    )"
}
