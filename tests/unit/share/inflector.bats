load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"

load "$_SHEF_ROOT/share/install/functions"
load "$_SHEF_ROOT/share/inflector"

setup() {
    # shellcheck disable=SC1091
    . shellmock
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "inflector:recipe_path_to_name outputs the recipe name with vendor from recipe's full path" {
    # GIVEN
    RECIPE_PATH="/home/johndoe/.config/shef/recipes/ca/my-recipe"

    # WHEN
    run inflector:recipe_path_to_name "$RECIPE_PATH"

    # THEN
    assert_success
    assert_output ca/my-recipe
}

@test "inflector:recipe_path_to_name fails if no RECIPE_PATH given" {
    # GIVEN
    RECIPE_PATH=

    # WHEN
    run inflector:recipe_path_to_name "$RECIPE_PATH"

    # THEN
    assert_failure
}


@test "inflector:name_to_function_name fails if no RECIPE_PATH given" {
    # GIVEN
    NAME=

    # WHEN
    run inflector:name_to_function_name "$NAME"

    # THEN
    assert_failure
}

@test "inflector:name_to_function_name outputs the function name from the recipe's full path" {
    # GIVEN
    NAME="ca/my-recipe"

    # WHEN
    run inflector:name_to_function_name "$NAME"

    # THEN
    assert_success
    assert_output MY
}



