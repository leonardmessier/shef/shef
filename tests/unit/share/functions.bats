load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_SHEF_ROOT/tests/unit/helpers/assert"

load "$_SHEF_ROOT/share/functions"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    TEST_TEMP_DIR="$(mktemp -d -- 'share_functions-XXXXXXXXXX')"
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi

    rm -rf "$TEST_TEMP_DIR"
}

@test "change_directory returns 1 if no directory is given as input" {
    # WHEN
    run change_directory

    # THEN
    assert_failure 1
    assert_output --partial "First argument to change_directory is mandatory"
}

@test "change_directory returns 2 if input directory does not exist" {
    # WHEN
    run change_directory /home/linuxbrew/any-directory

    # THEN
    assert_failure 2
    assert_output --partial "Directory « /home/linuxbrew/any-directory » does not exist or is not a directory"
}

@test "change_directory returns 2 if input directory is not a directory" {
    # GIVEN
    touch "$TEST_TEMP_DIR/a-file"

    # WHEN
    run change_directory "$TEST_TEMP_DIR/a-file"

    # THEN
    assert_failure 2
    assert_output --partial "Directory « $TEST_TEMP_DIR/a-file » does not exist or is not a directory"
}

@test "change_directory returns 0 if changing directory succeeded" {
    # GIVEN
    mkdir "$TEST_TEMP_DIR/a-directory"
    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run change_directory "$TEST_TEMP_DIR/a-directory"

    # THEN
    assert_success
    assert_output --partial "Move to directory « $TEST_TEMP_DIR/a-directory »"
}

@test "_shef_block_has_title returns 1 if first block argument and option are equal" {
    # GIVEN
    FIRST_BLOCK_ARGUMENT="my/first/argument"
    OPTION="my/first/argument"

    # WHEN
    run _shef_block_has_title $FIRST_BLOCK_ARGUMENT $OPTION

    # THEN
    assert_failure 1
}

@test "_shef_block_has_title returns 1 if first block argument and option differ because first block argument is a flag" {
    # GIVEN
    FIRST_BLOCK_ARGUMENT="--my-option"
    OPTION="my/first/option-value"

    # WHEN
    run _shef_block_has_title $FIRST_BLOCK_ARGUMENT $OPTION

    # THEN
    assert_failure 1
}

@test "_shef_block_has_title returns 0 if first block argument and option differ" {
    # GIVEN
    FIRST_BLOCK_ARGUMENT="My block title"
    OPTION="my/first/option-value"

    # WHEN
    run _shef_block_has_title "$FIRST_BLOCK_ARGUMENT" $OPTION

    # THEN
    assert_success
}
