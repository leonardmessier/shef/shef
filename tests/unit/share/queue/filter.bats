load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_SHEF_ROOT/tests/unit/helpers/assert"

load "$_SHEF_ROOT/share/functions"
load "$_SHEF_ROOT/share/queue/module"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    if [ -z "$TEST_FUNCTION" ]; then
        rm -rf "$SHEF_CACHE_DIR/queue"
        echo '' > "$SHEF_LOG_FILE"
    fi
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "queue_filter:command extracts command from a queue line" {
    # GIVEN
    LINE="[x] link Link2 --target /root/my-target --to /root/my-symlink"

    # WHEN
    run queue_filter:command "$LINE"

    # THEN
    assert_success
    assert_output "link Link2 --target /root/my-target --to /root/my-symlink"
}

@test "queue_filter:resource extracts resource from a command" {
    # GIVEN
    LINE="link Link2 --target /root/my-target --to /root/my-symlink"

    # WHEN
    run queue_filter:resource "$LINE"

    # THEN
    assert_success
    assert_output "link"
}

@test "queue_filter:status --skip marks a queue item as skipped" {
    # GIVEN
    LINE="link Link2 --target /root/my-target --to /root/my-symlink"

    # WHEN
    run queue_filter:status "$LINE" --skip

    # THEN
    assert_success
    assert_output "[-] link Link2 --target /root/my-target --to /root/my-symlink"
}

@test "queue_filter:status --todo marks a queue item as to be done" {
    # GIVEN
    LINE="link Link2 --target /root/my-target --to /root/my-symlink"

    # WHEN
    run queue_filter:status "$LINE" --todo

    # THEN
    assert_success
    assert_output "[ ] link Link2 --target /root/my-target --to /root/my-symlink"
}

@test "queue_filter:done marks a queue item as done" {
    # GIVEN
    LINE="link Link2 --target /root/my-target --to /root/my-symlink"

    # WHEN
    run queue_filter:status "$LINE" --done

    # THEN
    assert_success
    assert_output "[x] link Link2 --target /root/my-target --to /root/my-symlink"
}

@test "queue_filter:status --rollback marks a queue item as rolled back" {
    # GIVEN
    LINE="link Link2 --target /root/my-target --to /root/my-symlink"

    # WHEN
    run queue_filter:status "$LINE" --rollback

    # THEN
    assert_success
    assert_output "[o] link Link2 --target /root/my-target --to /root/my-symlink"
}

@test "queue_filter:status --fail marks a queue item as a failure" {
    # GIVEN
    LINE="link Link2 --target /root/my-target --to /root/my-symlink"

    # WHEN
    run queue_filter:status "$LINE" --fail

    # THEN
    assert_success
    assert_output "[!] link Link2 --target /root/my-target --to /root/my-symlink"
}

@test "queue_filter:command_to_eval produces final command to evaluate" {
    # GIVEN
    LINE="link Link2 --target /root/my-target --to /root/my-symlink"
    REVERSE=0

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 1

    # WHEN
    run queue_filter:command_to_eval "$LINE" "$REVERSE"

    # THEN
    assert_success
    assert_output "shef link Link2 --target /root/my-target --to /root/my-symlink"
}

@test "queue_filter:command_to_eval produces final command to evaluate with reverse on" {
    # GIVEN
    LINE="link Link2 --target /root/my-target --to /root/my-symlink"
    REVERSE=1

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 1

    # WHEN
    run queue_filter:command_to_eval "$LINE" $REVERSE

    # THEN
    assert_success
    assert_output "shef link Link2 --target /root/my-target --to /root/my-symlink --rollback"
}

@test "queue_filter:command_to_eval produces final command to evaluate and honours SHEF_BINARY env variable" {
    # GIVEN
    LINE="link Link2 --target /root/my-target --to /root/my-symlink"
    REVERSE=0
    export SHEF_BINARY="./bin/shef"

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 1

    # WHEN
    run queue_filter:command_to_eval "$LINE" $REVERSE

    # THEN
    assert_success
    assert_output "./bin/shef link Link2 --target /root/my-target --to /root/my-symlink"
}

@test "queue_filter:command_to_eval produces final command to evaluate with internal function if it exists" {
    # GIVEN
    LINE="link Link2 --target /root/my-target --to /root/my-symlink"
    REVERSE=0

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 0

    # WHEN
    run queue_filter:command_to_eval "$LINE" $REVERSE

    # THEN
    assert_success
    assert_output "shef_link Link2 --target /root/my-target --to /root/my-symlink"
}



