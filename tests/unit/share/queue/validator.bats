load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_SHEF_ROOT/tests/unit/helpers/assert"

load "$_SHEF_ROOT/share/functions"
load "$_SHEF_ROOT/share/queue/module"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    if [ -z "$TEST_FUNCTION" ]; then
        rm -rf "$SHEF_CACHE_DIR/queue"
        echo '' > "$SHEF_LOG_FILE"
    fi
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "validator:process_line exit with 1 for an empty line" {
    # GIVEN
    LINE=""
    declare -i REVERSE=0

    # WHEN
    run queue_validator:process_line "$LINE" $REVERSE

    # THEN
    assert_failure 1
    assert_log_contains "Line is empty"
}

@test "validator:process_line exit with 2 for a line starting with [ ] and reverse on" {
    # GIVEN
    LINE="[ ] link Link2 --target /root/my-target --to /root/my-symlink"
    declare -i REVERSE=1

    # WHEN
    run queue_validator:process_line "$LINE" $REVERSE

    # THEN
    assert_failure 2
    assert_log_contains "Line « \[ \] link Link2 --target /root/my-target --to /root/my-symlink » is not to be rolled back because it was not executed in the first place"
}

@test "validator:process_line exit with 3 for a line starting with [x]" {
    # GIVEN
    LINE="[x] link Link2 --target /root/my-target --to /root/my-symlink"
    declare -i REVERSE=0

    # WHEN
    run queue_validator:process_line "$LINE" $REVERSE

    # THEN
    assert_failure 3
    assert_log_contains "Line « \[x\] link Link2 --target /root/my-target --to /root/my-symlink » is marked as executed"
}

@test "validator:process_line exit with 4 for a line starting with [o] and reverse on" {
    # GIVEN
    LINE="[o] link Link2 --target /root/my-target --to /root/my-symlink"
    declare -i REVERSE=1

    # WHEN
    run queue_validator:process_line "$LINE" $REVERSE

    # THEN
    assert_failure 4
    assert_log_contains "Line « \[o\] link Link2 --target /root/my-target --to /root/my-symlink » is marked as rolled back"
}

@test "validator:process_line exit with 5 for a line starting with [-] and reverse on" {
    # GIVEN
    LINE="[-] link Link2 --target /root/my-target --to /root/my-symlink"
    declare -i REVERSE=1

    # WHEN
    run queue_validator:process_line "$LINE" $REVERSE

    # THEN
    assert_failure 5
    assert_log_contains "Line « \[-\] link Link2 --target /root/my-target --to /root/my-symlink » is marked as skipped"
}

@test "validator:validate_resource rejects line with an valid resource" {
    # GIVEN
    LINE="[ ] pink Link2 --target /root/my-target --to /root/my-symlink"

    # WHEN
    run queue_validator:validate_resource "$LINE"

    # THEN
    assert_failure 1
    assert_log_contains "Line « \[ \] pink Link2 --target /root/my-target --to /root/my-symlink » is invalid: there is no « shef-pink » resource"
}

@test "validator:validate_resource accepts line with valid resource" {
    # GIVEN
    LINE="[ ] link Link2 --target /root/my-target --to /root/my-symlink"

    # WHEN
    run queue_validator:validate_resource "$LINE"

    # THEN
    assert_success 0
}
