load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_SHEF_ROOT/tests/unit/helpers/assert"

load "$_SHEF_ROOT/share/functions"
load "$_SHEF_ROOT/share/queue/module"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    if [ -z "$TEST_FUNCTION" ]; then
        rm -rf "$SHEF_CACHE_DIR/queue"
        echo '' > "$SHEF_LOG_FILE"
    fi
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}


@test "_shef_queue:create returns 1 if no recipe is given" {
    # WHEN
    run _shef_queue:create

    # THEN
    assert_failure 1
    assert_output --partial "_shef_queue:create requires a recipe name as first argument"
}

@test "_shef_queue:create returns 2 if queue directory creation failed" {
    # GIVEN
    RECIPE=jrnl

    shellmock_expect mkdir \
        --match '-p /tmp/.cache/shef/queue' \
        --status 1

    # WHEN
    run _shef_queue:create "$RECIPE"

    # THEN
    assert_failure 2
    assert_output --partial "_shef_queue:create creating base directory failed"
}

@test "_shef_queue:create returns 3 if queue file creation fails" {
    # GIVEN
    RECIPE=jrnl

    shellmock_expect mkdir \
        --match '-p /tmp/.cache/shef/queue' \
        --status 0

    shellmock_expect touch \
        --match '/tmp/.cache/shef/queue/jrnl.queue' \
        --status 1

    # WHEN
    run _shef_queue:create "$RECIPE"

    # THEN
    assert_failure 3
    assert_output --partial "_shef_queue:create creating queue file /tmp/.cache/shef/queue/jrnl.queue failed"
}

@test "_shef_queue:create returns 0 if queue file creation succeeds" {
    # GIVEN
    RECIPE=jrnl

    shellmock_expect mkdir \
        --match '-p /tmp/.cache/shef/queue' \
        --status 0

    shellmock_expect touch \
        --match '/tmp/.cache/shef/queue/jrnl.queue' \
        --status 0

    export SHEF_VERY_VERBOSE=true
    export SHEF_QUIET=false

    # WHEN
    run _shef_queue:create "$RECIPE"

    # THEN
    assert_success
    assert_output --partial "_shef_queue:create creating queue file /tmp/.cache/shef/queue/jrnl.queue succeeded"
}

@test "_shef_queue:add returns 1 if no command is given as input" {
    # WHEN
    run _shef_queue:add

    # THEN
    assert_failure 1
    assert_output --partial "_shef_queue:add requires a command as first argument"
}

@test "_shef_queue:add returns 2 if no command is given as input" {
    # GIVEN
    RECIPE=jrnl

    # WHEN
    run _shef_queue:add "$RECIPE"

    # THEN
    assert_failure 2
    assert_output --partial "_shef_queue:add requires a command as second argument"
}

@test "_shef_queue:add returns 0 if enqueuing command succeeds" {
    # GIVEN
    RECIPE=jrnl
    COMMAND="shef link \"Create link\" --target /root/link-target --to /root/link-to"

    # WHEN
    run _shef_queue:add "$RECIPE" "$COMMAND"

    # THEN
    assert_success
    assert_file_exist "/tmp/.cache/shef/queue/jrnl.queue"
    assert_file_is "/tmp/.cache/shef/queue/jrnl.queue" "[ ] shef link \"Create link\" --target /root/link-target --to /root/link-to"
}

@test "_shef_queue:run returns 1 if recipe argument is missing" {
    # WHEN
    run _shef_queue:run

    # THEN
    assert_failure 1
    assert_output --partial "_shef_queue:run requires a recipe as first argument"
}

@test "_shef_queue:run returns 2 if queue file could not be found" {
    # GIVEN
    RECIPE=jrnl

    # WHEN
    run _shef_queue:run "$RECIPE"

    # THEN
    assert_failure 2
    assert_output --partial "_shef_queue:run could not find queue file /tmp/.cache/shef/queue/jrnl.queue"
}

@test "_shef_queue:run executes command in order" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    QUEUED_COMMANDS="$(cat <<-EOF
[ ] directory Directory --path /root/my/great/dir --parents
[ ] link Link --target /root/link-target --to /root/link-to
EOF
    )"
    echo -e "$QUEUED_COMMANDS" > "/tmp/.cache/shef/queue/jrnl.queue"

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 1

    shellmock_expect grep \
        --match '"export -f shef_directory" /code/libexec/shef-directory' \
        --status 1

    shellmock_expect shef \
        --match 'directory Directory --path /root/my/great/dir --parents' \
        --status 0

    shellmock_expect shef \
        --match 'link Link --target /root/link-target --to /root/link-to' \
        --status 0

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:run "$RECIPE"

    cat /tmp/shef.log
    # THEN
    assert_success
    assert_log_contains "Line « \[ \] directory Directory --path /root/my/great/dir --parents » execution succeeded with reverse mode 0"
    assert_log_contains "Line « \[ \] link Link --target /root/link-target --to /root/link-to » execution succeeded with reverse mode 0"
    assert_file_is "/tmp/.cache/shef/queue/jrnl.queue" "$(cat <<EOF
[x] directory Directory --path /root/my/great/dir --parents
[x] link Link --target /root/link-target --to /root/link-to
EOF
    )"

    shellmock_verify
    [ "${capture[0]}" = 'grep-stub "export -f shef_directory" /code/libexec/shef-directory' ]
    [ "${capture[1]}" = 'shef-stub directory Directory --path /root/my/great/dir --parents' ]
    [ "${capture[2]}" = 'grep-stub "export -f shef_link" /code/libexec/shef-link' ]
    [ "${capture[3]}" = 'shef-stub link Link --target /root/link-target --to /root/link-to' ]
}

@test "_shef_queue:run stops at first error in order" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    QUEUED_COMMANDS="$(cat <<-EOF
[ ] directory Directory --path /root/my/great/dir --parents
[ ] link Link --target /root/link-target --to /root/link-to
[ ] link Link --target /root/link-target-2 --to /root/link-to-2
EOF
    )"
    echo -e "$QUEUED_COMMANDS" > "/tmp/.cache/shef/queue/jrnl.queue"

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 1

    shellmock_expect grep \
        --match '"export -f shef_directory" /code/libexec/shef-directory' \
        --status 1

    shellmock_expect shef \
        --match 'directory Directory --path /root/my/great/dir --parents' \
        --status 0

    shellmock_expect shef \
        --match 'link Link --target /root/link-target --to /root/link-to' \
        --status 1

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:run "$RECIPE"

    # THEN
    assert_failure 1
    assert_log_contains "Line « \[ \] directory Directory --path /root/my/great/dir --parents » execution succeeded with reverse mode 0"
    assert_log_contains "Line « \[ \] link Link --target /root/link-target --to /root/link-to » execution failed with reverse mode 0"
    assert_file_is "/tmp/.cache/shef/queue/jrnl.queue" "$(cat <<EOF
[x] directory Directory --path /root/my/great/dir --parents
[!] link Link --target /root/link-target --to /root/link-to
[ ] link Link --target /root/link-target-2 --to /root/link-to-2
EOF
    )"

    shellmock_verify
    echo "${capture[2]}"
    [ "${capture[0]}" = 'grep-stub "export -f shef_directory" /code/libexec/shef-directory' ]
    [ "${capture[1]}" = 'shef-stub directory Directory --path /root/my/great/dir --parents' ]
    [ "${capture[2]}" = 'grep-stub "export -f shef_link" /code/libexec/shef-link' ]
    [ "${capture[3]}" = 'shef-stub link Link --target /root/link-target --to /root/link-to' ]
}

@test "_shef_queue:run executes command in reverse order if rollback is given" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    QUEUED_COMMANDS="$(cat <<-EOF
[x] directory Directory --path /root/my/great/dir --parents
[x] link Link --target /root/link-target --to /root/link-to
EOF
    )"
    echo -e "$QUEUED_COMMANDS" > "/tmp/.cache/shef/queue/jrnl.queue"

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 1

    shellmock_expect grep \
        --match '"export -f shef_directory" /code/libexec/shef-directory' \
        --status 1

    shellmock_expect shef \
        --match 'directory Directory --path /root/my/great/dir --parents --rollback' \
        --status 0

    shellmock_expect shef \
        --match 'link Link --target /root/link-target --to /root/link-to --rollback' \
        --status 0

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:run "$RECIPE" 1

    # THEN
    assert_success
    assert_log_contains "Line « \[x\] link Link --target /root/link-target --to /root/link-to » execution succeeded with reverse mode 1"
    assert_log_contains "Line « \[x\] directory Directory --path /root/my/great/dir --parents » execution succeeded with reverse mode 1"
    assert_file_is "/tmp/.cache/shef/queue/jrnl.queue" "$(cat <<EOF
[o] directory Directory --path /root/my/great/dir --parents
[o] link Link --target /root/link-target --to /root/link-to
EOF
    )"

    shellmock_verify
    [ "${capture[0]}" = 'grep-stub "export -f shef_link" /code/libexec/shef-link' ]
    [ "${capture[1]}" = 'shef-stub link Link --target /root/link-target --to /root/link-to --rollback' ]
    [ "${capture[2]}" = 'grep-stub "export -f shef_directory" /code/libexec/shef-directory' ]
    [ "${capture[3]}" = 'shef-stub directory Directory --path /root/my/great/dir --parents --rollback' ]
}

@test "_shef_queue:run stops at first error in reverse order" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    QUEUED_COMMANDS="$(cat <<-EOF
[x] directory Directory --path /root/my/great/dir --parents
[x] link Link --target /root/link-target --to /root/link-to
[x] link Link --target /root/link-target-2 --to /root/link-to-2
EOF
    )"
    echo -e "$QUEUED_COMMANDS" > "/tmp/.cache/shef/queue/jrnl.queue"

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 1

    shellmock_expect grep \
        --match '"export -f shef_directory" /code/libexec/shef-directory' \
        --status 1

    shellmock_expect shef \
        --match 'link Link --target /root/link-target-2 --to /root/link-to-2 --rollback' \
        --status 0

    shellmock_expect shef \
        --match 'link Link --target /root/link-target --to /root/link-to --rollback' \
        --status 1

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:run "$RECIPE" 1

    # THEN
    #assert_failure 1
    shellmock_verify
    assert_equal "${capture[0]}" 'grep-stub "export -f shef_link" /code/libexec/shef-link'
    assert_equal "${capture[1]}" 'shef-stub link Link --target /root/link-target-2 --to /root/link-to-2 --rollback'
    assert_equal "${capture[2]}" 'grep-stub "export -f shef_link" /code/libexec/shef-link'
    assert_equal "${capture[3]}" 'shef-stub link Link --target /root/link-target --to /root/link-to --rollback'

    assert_log_contains "Line « \[x\] link Link --target /root/link-target --to /root/link-to » execution failed with reverse mode 1"
    assert_log_contains "Line « \[x\] link Link --target /root/link-target-2 --to /root/link-to-2 » execution succeeded with reverse mode 1"
    assert_file_is "/tmp/.cache/shef/queue/jrnl.queue" "$(cat <<EOF
[ ] directory Directory --path /root/my/great/dir --parents
[!] link Link --target /root/link-target --to /root/link-to
[o] link Link --target /root/link-target-2 --to /root/link-to-2
EOF
    )"


}

@test "_shef_queue:run starts at first failed step and rewinds to the start" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    QUEUED_COMMANDS="$(cat <<-EOF
[x] directory Directory --path /root/my/great/dir --parents
[x] link Link --target /root/link-target --to /root/link-to
[x] link Link2 --target /root/link-target-2 --to /root/link-to-2
[x] directory Directory2 --path /root/my/great/dir2 --parents
[!] link Link3 --target /root/link-target-3 --to /root/link-to-3
[ ] link Link4 --target /root/link-target-4 --to /root/link-to-4
[ ] link Link5 --target /root/link-target-5 --to /root/link-to-5
[ ] directory Directory3 --path /root/my/great/dir3 --parents
EOF
    )"
    echo -e "$QUEUED_COMMANDS" > "/tmp/.cache/shef/queue/jrnl.queue"

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 1

    shellmock_expect grep \
        --match '"export -f shef_directory" /code/libexec/shef-directory' \
        --status 1

    shellmock_expect shef \
        --match 'link Link3 --target /root/link-target-3 --to /root/link-to-3 --rollback' \
        --status 0

    shellmock_expect shef \
        --match 'directory Directory2 --path /root/my/great/dir2 --parents --rollback' \
        --status 0

    shellmock_expect shef \
        --match 'link Link2 --target /root/link-target-2 --to /root/link-to-2 --rollback' \
        --status 0

    shellmock_expect shef \
        --match 'link Link --target /root/link-target --to /root/link-to --rollback' \
        --status 0

    shellmock_expect shef \
        --match 'directory Directory --path /root/my/great/dir --parents --rollback' \
        --status 0

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:run "$RECIPE" 1

    # THEN
    assert_success
    shellmock_verify

    assert_equal "${capture[0]}" 'grep-stub "export -f shef_link" /code/libexec/shef-link'
    assert_equal "${capture[1]}" 'shef-stub link Link3 --target /root/link-target-3 --to /root/link-to-3 --rollback'
    assert_log_contains "Line « \[!\] link Link3 --target /root/link-target-3 --to /root/link-to-3 » execution succeeded with reverse mode 1"

    assert_equal "${capture[2]}" 'grep-stub "export -f shef_directory" /code/libexec/shef-directory'
    assert_equal "${capture[3]}" 'shef-stub directory Directory2 --path /root/my/great/dir2 --parents --rollback'
    assert_log_contains "Line « \[x\] directory Directory2 --path /root/my/great/dir2 --parents » execution succeeded with reverse mode 1"

    assert_equal "${capture[4]}" 'grep-stub "export -f shef_link" /code/libexec/shef-link'
    assert_equal "${capture[5]}" 'shef-stub link Link2 --target /root/link-target-2 --to /root/link-to-2 --rollback'
    assert_log_contains "Line « \[x\] link Link2 --target /root/link-target-2 --to /root/link-to-2 » execution succeeded with reverse mode 1"

    assert_equal "${capture[6]}" 'grep-stub "export -f shef_link" /code/libexec/shef-link'
    assert_equal "${capture[7]}" 'shef-stub link Link --target /root/link-target --to /root/link-to --rollback'
    assert_log_contains "Line « \[x\] link Link --target /root/link-target --to /root/link-to » execution succeeded with reverse mode 1"

    assert_equal "${capture[8]}" 'grep-stub "export -f shef_directory" /code/libexec/shef-directory'
    assert_equal "${capture[9]}" 'shef-stub directory Directory --path /root/my/great/dir --parents --rollback'
    assert_log_contains "Line « \[x\] directory Directory --path /root/my/great/dir --parents » execution succeeded with reverse mode 1"

    assert_file_is "/tmp/.cache/shef/queue/jrnl.queue" "$(cat <<EOF
[o] directory Directory --path /root/my/great/dir --parents
[o] link Link --target /root/link-target --to /root/link-to
[o] link Link2 --target /root/link-target-2 --to /root/link-to-2
[o] directory Directory2 --path /root/my/great/dir2 --parents
[o] link Link3 --target /root/link-target-3 --to /root/link-to-3
EOF
    )"
}

@test "_shef_queue:reset fails with exit code 1 if recipe argument is missing" {
    # WHEN
    run _shef_queue:reset

    # THEN
    assert_failure 1
    assert_output --partial "_shef_queue:reset requires a recipe name as first argument"
}

@test "_shef_queue:reset fails with exit code 2 if queue file does not exist" {
    # GIVEN
    RECIPE=jrnl

    # WHEN
    run _shef_queue:reset "$RECIPE"

    # THEN
    assert_failure 2
    assert_output --partial "queue file /tmp/.cache/shef/queue/jrnl.queue does not exist"
}

@test "_shef_queue:reset will succeed by emptying queue file" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    QUEUED_COMMANDS="$(cat <<-EOF
[x] shef directory Directory --path /root/my/great/dir --parents
[x] shef link Link --target /root/link-target --to /root/link-to
[x] shef link Link2 --target /root/link-target-2 --to /root/link-to-2
[x] shef directory Directory2 --path /root/my/great/dir2 --parents
EOF
    )"
    echo -e "$QUEUED_COMMANDS" > "/tmp/.cache/shef/queue/jrnl.queue"

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:reset "$RECIPE"

    # THEN
    assert_success
    assert_file_is_empty "/tmp/.cache/shef/queue/jrnl.queue"
    assert_output --partial "queue file /tmp/.cache/shef/queue/jrnl.queue was reset"
}

@test "_shef_queue:run exits early if file is empty" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    touch "/tmp/.cache/shef/queue/jrnl.queue"

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:run "$RECIPE"

    # THEN
    assert_success
    assert_log_contains "Queue file /tmp/.cache/shef/queue/jrnl.queue is empty. Nothing to do"
}

@test "_shef_queue:run skips line if the command is not a shef command and errors out with 3" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    QUEUED_COMMANDS="$(cat <<-EOF
[ ] directory Directory --path /root/my/great/dir --parents
[ ] rm -rf /tmp/.cache/shef
EOF
    )"
    echo -e "$QUEUED_COMMANDS" > "/tmp/.cache/shef/queue/jrnl.queue"

    shellmock_expect grep \
        --match '"export -f shef_directory" /code/libexec/shef-directory' \
        --status 1

    shellmock_expect shef \
        --match 'directory Directory --path /root/my/great/dir --parents' \
        --status 0

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:run "$RECIPE"

    # THEN
    assert_failure 3

    assert_log_contains "Line « \[ \] rm -rf /tmp/.cache/shef » is invalid: there is no « shef-rm » resource"
    assert_file_is "/tmp/.cache/shef/queue/jrnl.queue" "$(cat <<EOF
[x] directory Directory --path /root/my/great/dir --parents
[-] rm -rf /tmp/.cache/shef
EOF
    )"

}

@test "_shef_queue:run skips line if the task was already done" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    QUEUED_COMMANDS="$(cat <<-EOF
[x] directory Directory --path /root/my/great/dir --parents
[x] link Link --target /root/link-target --to /root/link-to
[ ] link Link2 --target /root/link-target-2 --to /root/link-to-2
[ ] directory Directory2 --path /root/my/great/dir2 --parents
EOF
    )"
    echo -e "$QUEUED_COMMANDS" > "/tmp/.cache/shef/queue/jrnl.queue"

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 1

    shellmock_expect shef \
        --match 'link Link2 --target /root/link-target-2 --to /root/link-to-2' \
        --status 0

    shellmock_expect shef \
        --match 'directory Directory2 --path /root/my/great/dir2 --parents' \
        --status 0

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:run "$RECIPE"

    # THEN
    assert_success
    assert_log_contains "Line « \[x\] directory Directory --path /root/my/great/dir --parents » is marked as executed"
    assert_log_contains "Line « \[x\] link Link --target /root/link-target --to /root/link-to » is marked as executed"

    assert_file_is "/tmp/.cache/shef/queue/jrnl.queue" "$(cat <<EOF
[x] directory Directory --path /root/my/great/dir --parents
[x] link Link --target /root/link-target --to /root/link-to
[x] link Link2 --target /root/link-target-2 --to /root/link-to-2
[x] directory Directory2 --path /root/my/great/dir2 --parents

EOF
    )"
}

@test "_shef_queue:run skips line if the task was already rolled back" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    QUEUED_COMMANDS="$(cat <<-EOF
[x] directory Directory2 --path /root/my/great/dir2 --parents
[x] link Link2 --target /root/link-target-2 --to /root/link-to-2
[o] link Link --target /root/link-target --to /root/link-to
[o] directory Directory --path /root/my/great/dir --parents
EOF
    )"
    echo -e "$QUEUED_COMMANDS" > "/tmp/.cache/shef/queue/jrnl.queue"

    shellmock_expect grep \
        --match '"export -f shef_link" /code/libexec/shef-link' \
        --status 1

    shellmock_expect shef \
        --match 'link Link2 --target /root/link-target-2 --to /root/link-to-2 --rollback' \
        --status 0

    shellmock_expect shef \
        --match 'directory Directory2 --path /root/my/great/dir2 --parents --rollback' \
        --status 0

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:run "$RECIPE" 1

    # THEN
    assert_success
    assert_log_contains "Line « \[o\] link Link --target /root/link-target --to /root/link-to » is marked as rolled back"
    assert_log_contains "Line « \[o\] directory Directory --path /root/my/great/dir --parents » is marked as rolled back"

    assert_file_is "/tmp/.cache/shef/queue/jrnl.queue" "$(cat <<EOF
[o] directory Directory2 --path /root/my/great/dir2 --parents
[o] link Link2 --target /root/link-target-2 --to /root/link-to-2
[o] link Link --target /root/link-target --to /root/link-to
[o] directory Directory --path /root/my/great/dir --parents
EOF
    )"


}

@test "_shef_queue:run skips already skipped files" {
    # GIVEN
    RECIPE=jrnl
    mkdir -p "/tmp/.cache/shef/queue"
    QUEUED_COMMANDS="$(cat <<-EOF
[x] directory Directory2 --path /root/my/great/dir2 --parents
[x] link Link2 --target /root/link-target-2 --to /root/link-to-2
[-] rm -rf /root/.config/shef/config
[x] directory Directory --path /root/my/great/dir --parents
EOF
    )"
    echo -e "$QUEUED_COMMANDS" > "/tmp/.cache/shef/queue/jrnl.queue"

    shellmock_expect grep \
        --match '"export -f shef_directory" /code/libexec/shef-directory' \
        --status 1

    shellmock_expect shef \
        --match 'directory Directory --path /root/my/great/dir --parents --rollback' \
        --status 0

    shellmock_expect shef \
        --match 'link Link2 --target /root/link-target-2 --to /root/link-to-2 --rollback' \
        --status 0

    shellmock_expect shef \
        --match 'directory Directory2 --path /root/my/great/dir2 --parents --rollback' \
        --status 0

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run _shef_queue:run "$RECIPE" 1

    # THEN
    assert_success
    assert_log_contains "Line « \[-\] rm -rf /root/.config/shef/config » is marked as skipped"

    assert_file_is "/tmp/.cache/shef/queue/jrnl.queue" "$(cat <<EOF
[o] directory Directory2 --path /root/my/great/dir2 --parents
[o] link Link2 --target /root/link-target-2 --to /root/link-to-2
[-] rm -rf /root/.config/shef/config
[o] directory Directory --path /root/my/great/dir --parents
EOF

    shellmock_verify
    [ "${capture[0]}" = 'shef-stub directory Directory --path /root/my/great/dir --parents' ]
    [ "${capture[1]}" = 'shef-stub link Link2 --target /root/link-target-2 --to /root/link-to-2' ]
    [ "${capture[2]}" = 'shef-stub directory Directory2 --path /root/my/great/dir2 --parents' ]
    )"
}
