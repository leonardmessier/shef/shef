#!/usr/bin/env bats

load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_SHEF_ROOT/tests/unit/helpers/assert"

load "./share/template/functions"

@test "_build_shell_format transforms a list of variables to a format usable by envsubst" {
  run _build_shell_format 'VAR1=value1;VAR2=value2'
  [ "$output" = '$VAR1:$VAR2' ]
}

@test "_build_esh_command transforms a list of arguments to an esh command line" {
  run _build_esh_command_arguments file.conf.esh "VAR1=first;VAR2=second" file.conf
  [ "$output" = '-o file.conf file.conf.esh VAR1=first VAR2=second' ]
}

@test "_build_esh_command transforms a list of arguments to an esh command line without output file" {
  run _build_esh_command_arguments file.conf.esh "\"VAR1=first;VAR2=second\""
  assert_equal "$output" 'file.conf.esh VAR1=first VAR2=second'
  [ "$output" = 'file.conf.esh VAR1=first VAR2=second' ]
}
