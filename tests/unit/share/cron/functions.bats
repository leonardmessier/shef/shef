load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"

load "$_SHEF_ROOT/tests/unit/helpers/assert"
load "$_SHEF_ROOT/share/cron/functions"

setup() {
    # shellcheck disable=SC1091
    . shellmock
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "_cron_command_exists returns 1 if command in command line is not found" {
    # WHEN
    run _cron_command_exists my-command with parameters

    # THEN
    assert_failure 1
}

@test "_cron_command_exists returns 0 if command in command line is found" {
    # WHEN
    run _cron_command_exists cat /etc/issue

    # THEN
    assert_success
}

@test "_job_already_exists_with_same_schedule returns 0 if the job is already here" {
    # GIVEN
    shellmock_expect crontab \
        --match '-l' \
        --status 0 \
        --output "$(cat <<-EOF
* * * * * my-script
EOF
        )"

    # WHEN
    run  _job_already_exists_with_same_schedule 'my-script' '* * * * *'

    # THEN
    assert_success
    assert_log_contains "$(cat <<-EOF
Job '\* \* \* \* \* my-script' already exists. Nothing to do
EOF
    )"
}

@test "_job_already_exists_with_same_schedule returns 1 if the job does not exist" {
    # GIVEN
    shellmock_expect crontab \
        --match '-l' \
        --status 0 \
        --output "$(cat <<-EOF
* * * * * another-script
EOF
        )"

    # WHEN
    run  _job_already_exists_with_same_schedule 'my-script' '* * * * *'

    # THEN
    assert_failure 1
    assert_log_contains "$(cat <<-EOF
Job '\* \* \* \* \* my-script' does not exist
EOF
    )"
}

@test "_job_already_exists_with_another_schedule returns 0 if job with same command but with a different schedule exists" {
    # GIVEN
    shellmock_expect crontab \
        --match '-l' \
        --status 0 \
        --output "$(cat <<-EOF
* 13 * * 1 my-script
EOF
        )"

    # WHEN
    run  _job_already_exists_with_another_schedule 'my-script'

    # THEN
    assert_success
    assert_log_contains "$(cat <<-EOF
Command 'my-script' is already in crontab with a different schedule
EOF
    )"
}

@test "_job_already_exists_with_another_schedule returns 1 if job with different command and different schedule exists" {
    # GIVEN
    shellmock_expect crontab \
        --match '-l' \
        --status 0 \
        --output "$(cat <<-EOF
* * * * * another-script
EOF
        )"

    # WHEN
    run  _job_already_exists_with_another_schedule 'my-script'

    # THEN
    assert_failure 1
    assert_log_contains "$(cat <<-EOF
Command 'my-script' is not in crontab with any schedule
EOF
    )"
}

@test "_add_to_crontab adds a line to empty crontab" {
    # GIVEN
    shellmock_expect crontab \
        --match '-l' \
        --status 1 \
        --output 'No crontab for linuxbrew'

    # WHEN
    run _add_to_crontab 'my-script' '* * * * *'

    # THEN
    assert_success
    assert_output "$(cat <<-EOF
* * * * * my-script
EOF
    )"

}
@test "_add_to_crontab adds a line to existing crontab" {
    # GIVEN
    shellmock_expect crontab \
        --match '-l' \
        --status 0 \
        --output "$(cat <<-EOF
* * * * * another-script
EOF
        )"

    # WHEN
    run _add_to_crontab 'my-script' '* * * * *'

    # THEN
    assert_success
    assert_output "$(cat <<-EOF
* * * * * another-script
* * * * * my-script
EOF
    )"

}

@test "_update_crontab replaces a line with updated schedule" {
    # GIVEN
    shellmock_expect crontab \
        --match '-l' \
        --status 0 \
        --output "$(cat <<-EOF
* * * * * another-script
* * * * * my-script
EOF
        )"

    # WHEN
    run _update_crontab 'my-script' '* 13 * * 1'

    # THEN
    assert_success
    assert_output "$(cat <<-EOF
* * * * * another-script
* 13 * * 1 my-script
EOF
    )"

}

@test "_sanitize_job_schedule escapes characters" {
     # GIVEN
     JOB='* 13 * * 1 my-script'

     # WHEN
     run _sanitize_job_schedule "$JOB"

     assert_success
     assert_output '\* 13 \* \* 1 my-script'
  
}
