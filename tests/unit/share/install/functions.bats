load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"

load "$_SHEF_ROOT/share/install/functions"

setup() {
    # shellcheck disable=SC1091
    . shellmock
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "indent_built_recipe_instructions adds a four space wide indentation" {
    # GIVEN
    INSTRUCTION="$(cat <<-'INST'
_shef_queue:add git "$(cat <<-EOF
git "Retrieving git configuration" --repository "https://github.com/CaptainQuirk/.gitglobal.git" --destination "$SHEF_CONFIG_SRC_DIR/.gitglobal" --enable-submodules || return $?
EOF
INST
    )"

    # WHEN
    run indent_built_recipe_instructions "$INSTRUCTION" 4

    # THEN
    assert_success
    assert_output "$(cat <<-'INST'
    _shef_queue:add git "$(cat <<-EOF
git "Retrieving git configuration" --repository "https://github.com/CaptainQuirk/.gitglobal.git" --destination "$SHEF_CONFIG_SRC_DIR/.gitglobal" --enable-submodules || return $?
EOF
INST
    )"
}

