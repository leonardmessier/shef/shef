#!/usr/bin/env bats

load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"

load "$_SHEF_ROOT/share/install/ini-block-compiler"
load "$_SHEF_ROOT/share/inflector"

load "$_SHEF_ROOT/tests/unit/helpers/compile_ini_block"
load "$_SHEF_ROOT/tests/unit/helpers/assert"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    mkdir -p "$BATS_TMPDIR/.cache/shef"

    # Runtime shef configuration
    mkdir -p "$BATS_TMPDIR/.config/shef"
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi

    rm -rf "$BATS_TMPDIR/.cache/shef"
    rm -rf "$BATS_TMPDIR/.config/shef"
}

@test "it outputs an empty string if recipe does not contain ini file" {
    # GIVEN
    RECIPE_PATH="$BATS_TMPDIR/.config/shef/recipes/ca/noinifile-recipe"

    __compile:create_recipe_dir__ "ca/noinifile"

    # WHEN
    run ini_block_compiler compile "$RECIPE_PATH"

    # THEN
    assert_success
    assert_output ""
}

@test "It outputs an ini block with variables from original ini file" {

    # GIVEN
    RECIPE_PATH="$BATS_TMPDIR/.config/shef/recipes/ca/inifile-recipe"

    __compile:create_recipe_ini__ "ca/inifile-recipe" "$(cat <<-EOF
my_variable =
EOF
)"
    # EXPECT

    # vimdiff is called with 2 files
    # - The recipe's ini file that contains the variables without values
    # - A temp file that will be used as a final version
    shellmock_expect x-terminal-emulator \
        --type regex \
        --match "-e vimdiff $BATS_TMPDIR/.config/shef/recipes/ca/inifile-recipe/recipe.ini /tmp/tmp.*" \
        --status 0 \

    shellmock_expect parse-ini \
      --match "--local --uppercase --global-name MAIN --prefix SHEF_RECIPE_INIFILE $BATS_TMPDIR/.config/shef/runtime/ca/inifile-recipe/recipe.ini" \
      --output "$(cat <<-EOF
declare -l -A SHEF_RECIPE_INIFILE_MAIN
SHEF_RECIPE_INIFILE_MAIN[my_variable]=
EOF
)"

    # WHEN
    run ini_block_compiler compile "$RECIPE_PATH"

    assert_success
    assert_output <<EOF
    # Ini variables from $BATS_TMPDIR/.config/shef/runtime/ca/inifile-recipe/recipe.ini
    declare -l -A SHEF_RECIPE_INIFILE_MAIN
SHEF_RECIPE_INIFILE_MAIN[my_variable]=
EOF

    assert_file_is \
        "$BATS_TMPDIR/.cache/shef/ca/inifile-recipe/recipe.ini.md5" \
        "$(cat <<-EOF
82c67875d6aa2a2ae282f03e4737f01b  $BATS_TMPDIR/.config/shef/recipes/ca/inifile-recipe/recipe.ini
EOF
)"
    assert_dir_exists "$BATS_TMPDIR/.config/shef/runtime/ca/inifile-recipe"
    assert_file_is \
      "$BATS_TMPDIR/.config/shef/runtime/ca/inifile-recipe/recipe.ini" \
      "$(cat <<-EOF
my_variable =
EOF
)"
}

@test "It outputs an ini block with variables from ini file filled via the vimdiff program if recipe ini file was changed" {

    # GIVEN
    RECIPE_PATH="$BATS_TMPDIR/.config/shef/recipes/ca/updated-recipe"

    # recipe ini file has a different structure than runtime file
    # and runtime file has a value
    __compile:create_recipe_ini__ "ca/updated-recipe" "$(cat <<-EOF
my_variable =
my_other_variable =
EOF
)"
    __compile:create_runtime_dir__ "ca/updated-recipe" "$(cat <<-EOF
my_variable = 'My value'
EOF
)"

    __compile:create_md5_recipe_ini__ "ca/updated-recipe" "$(cat <<-EOF
3cf78a4a396dbd6523c269d9038b59dd  $BATS_TMPDIR/.config/shef/recipes/ca/updated-recipe/recipe.ini
EOF
)"

    # EXPECT

    # vimdiff is called with 3 files
    # - The recipe's ini file that contains the variables without values
    # - A temp file that will be used as a final version
    # - The recipe's runtime file, probably changed when recipe was first
    # installed
    shellmock_expect x-terminal-emulator \
        --type regex \
        --match "-e vimdiff $BATS_TMPDIR/.config/shef/recipes/ca/inifile-recipe/recipe.ini /tmp/tmp.* $BATS_TMPDIR/.config/shef/runtime/ca/inifile-recipe/recipe.ini" \
        --status 0 \

    shellmock_expect parse-ini \
      --match "--local --uppercase --global-name MAIN --prefix SHEF_RECIPE_UPDATED $BATS_TMPDIR/.config/shef/runtime/ca/updated-recipe/recipe.ini" \
      --output "$(cat <<-EOF
declare -l -A SHEF_RECIPE_UPDATED_MAIN
SHEF_RECIPE_UPDATED_MAIN[my_variable]=
SHEF_RECIPE_UPDATED_MAIN[my_other_variable]=
EOF
      )"

    # WHEN
    run ini_block_compiler compile "$RECIPE_PATH"

    assert_success

    # The output of the function contains two associative keys
    # declarations
    assert_output <<EOF
    # Ini variables from $BATS_TMPDIR/.config/shef/runtime/ca/updated-recipe/recipe.ini
    declare -l -A SHEF_RECIPE_UPDATED_MAIN
SHEF_RECIPE_UPDATED_MAIN[my_variable]=
SHEF_RECIPE_UPDATED_MAIN[my_other_variable]=
EOF

    # The recipe's ini checksum has changed
    assert_file_is \
        "$BATS_TMPDIR/.cache/shef/ca/updated-recipe/recipe.ini.md5" \
        "$(cat <<-EOF
3cf78a4a396dbd6523c269d9038b59dd  /tmp/.config/shef/recipes/ca/updated-recipe/recipe.ini
EOF
        )"

   # The recipe's runtime configuration has changed
   assert_file_is \
      "$SHEF_RUNTIME_DIR/ca/updated-recipe/recipe.ini" \
      "$(cat <<-EOF
my_variable =
my_other_variable =
EOF
      )"
}
