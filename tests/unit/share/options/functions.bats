load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_SHEF_ROOT/tests/unit/helpers/assert"

load "$_SHEF_ROOT/share/options/functions"
load "$_SHEF_ROOT/tests/unit/assets/shef-thing"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    export CAT=$(which cat)
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi

    rm -rf "$TEST_TEMP_DIR"
}

@test "options:parse reads a subcommand s file header and extracts options" {
    # GIVEN
    _SHEF_LIBEXEC_PATH="$_SHEF_ROOT/tests/unit/assets"
    export _SHEF_LIBEXEC_PATH

    # WHEN
    run shef_thing \
        "Creating repository" \
        --repository "https://github.com/CaptainQuirk/.vim" \
        --enable-submodules \
        --destination .vimtest \

    # THEN
    assert_success
    assert_output "$(cat <<-EOF
OPTS[repository] = https://github.com/CaptainQuirk/.vim
OPTS[destination] = .vimtest
OPTS[enable-submodules] = true
OPTS[rollback] = false
SHEF_LOG_FILE = /tmp/shef.log
EOF
    )"
}

@test "options:parse overwrites the default value for rollback" {
    # GIVEN
    _SHEF_LIBEXEC_PATH="$_SHEF_ROOT/tests/unit/assets"
    export _SHEF_LIBEXEC_PATH

    # WHEN
    run shef_thing \
        "Creating repository" \
        --repository "https://github.com/CaptainQuirk/.vim" \
        --enable-submodules \
        --destination .vimtest \
        --rollback

    # THEN
    assert_success
    assert_output "$(cat <<-EOF
OPTS[repository] = https://github.com/CaptainQuirk/.vim
OPTS[destination] = .vimtest
OPTS[enable-submodules] = true
OPTS[rollback] = true
SHEF_LOG_FILE = /tmp/shef.log
EOF
    )"
}

@test "options:parse handles global flags" {
    # GIVEN
    _SHEF_LIBEXEC_PATH="$_SHEF_ROOT/tests/unit/assets"
    export _SHEF_LIBEXEC_PATH

    # WHEN
    run shef_thing \
        "Creating repository" \
        --repository "https://github.com/CaptainQuirk/.vim" \
        --enable-submodules \
        --destination .vimtest \
        --if-not-installed git \
        --log-file /tmp/my-shef.log \
        --very-verbose

    # THEN
    assert_success
    assert_output "$(cat <<-EOF
OPTS[repository] = https://github.com/CaptainQuirk/.vim
OPTS[destination] = .vimtest
OPTS[enable-submodules] = true
OPTS[rollback] = false
SHEF_IF_NOT_INSTALLED = git
SHEF_LOG_FILE = /tmp/my-shef.log
SHEF_VERY_VERBOSE = true
EOF
    )"
}

@test "options:_from-flag-to-variable transforms a flag to a variable" {
    # GIVEN
    FLAG='--if-not-installed'

    run options:_from-flag-to-variable "$FLAG"

    assert_success
    assert_output "IF_NOT_INSTALLED"
}
