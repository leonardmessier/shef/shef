load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_SHEF_ROOT/tests/unit/helpers/assert"
load "$_SHEF_ROOT/share/cron/functions"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    mkdir -p "$BATS_TMPDIR/json"
    echo '' > /tmp/shef.log
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi

    rm -rf "$BATS_TMPDIR/json"
    echo '' > /tmp/shef.log
}

@test "shef-json errors out with 2 if action is not supported" {
    # GIVEN
    TARGET=$BATS_TMPDIR/json/config.json
    echo "$(cat <<-EOF
{
  "name": "my_project"
}
EOF
   )" > "$TARGET"

   PLUGIN=$BATS_TMPDIR/json/my_plugin.json
    echo "$(cat <<-EOF
{
  "theme": "molokai"
}
EOF
    )" > "$PLUGIN"

    # WHEN

    run bash ./libexec/shef-json "Title"\
      --action add \
      --property plugin.my_plugin \
      --from-file $PLUGIN \
      --target $TARGET \

    # THEN
    assert_failure 2
    assert_output --partial "Got add as action. Only « edit » is supported"
}

@test "shef-json errors out with 3 if property is missing" {
    # GIVEN
    TARGET=$BATS_TMPDIR/json/config.json
    echo "$(cat <<-EOF
{
  "name": "my_project"
}
EOF
   )" > "$TARGET"

   PLUGIN=$BATS_TMPDIR/json/my_plugin.json
    echo "$(cat <<-EOF
{
  "theme": "molokai"
}
EOF
    )" > "$PLUGIN"

    # WHEN

    run bash ./libexec/shef-json "Title"\
      --action edit \
      --from-file $PLUGIN \
      --target $TARGET \

    # THEN
    assert_failure 3
    assert_output --partial "--property option is missing"
}

@test "shef-json errors out with 4 if from-file is missing" {
    # GIVEN
    TARGET=$BATS_TMPDIR/json/config.json
    echo "$(cat <<-EOF
{
  "name": "my_project"
}
EOF
   )" > "$TARGET"

   PLUGIN=$BATS_TMPDIR/json/my_plugin.json
    echo "$(cat <<-EOF
{
  "theme": "molokai"
}
EOF
    )" > "$PLUGIN"

    # WHEN

    run bash ./libexec/shef-json "Title"\
      --action edit \
      --property plugin.my_plugin \
      --target $TARGET \

    # THEN
    assert_failure 4
    assert_output --partial "--from-file option is missing"
}

@test "shef-json errors out with 5 if from-file is not a valid filename" {
    # GIVEN
    TARGET=$BATS_TMPDIR/json/config.json
    echo "$(cat <<-EOF
{
  "name": "my_project"
}
EOF
   )" > "$TARGET"

   PLUGIN=$BATS_TMPDIR/json/my_plugin.json
    echo "$(cat <<-EOF
{
  "theme": "molokai"
}
EOF
    )" > "$PLUGIN"

    # WHEN

    run bash ./libexec/shef-json "Title"\
      --action edit \
      --property plugin.my_plugin \
      --from-file missing-file.json \
      --target $TARGET \

    # THEN
    assert_failure 5
    assert_output --partial "--from-file « missing-file.json » does not exist"
}

@test "shef-json errors out with 6 if from-file does not contain valid json" {
    # GIVEN
    TARGET=$BATS_TMPDIR/json/config.json
    echo "$(cat <<-EOF
{
  "name": "my_project"
}
EOF
   )" > "$TARGET"

   PLUGIN=$BATS_TMPDIR/json/my_plugin.json
    echo "$(cat <<-EOF
{
  "theme": "molokai
}
EOF
    )" > "$PLUGIN"

    # WHEN

    run bash ./libexec/shef-json "Title"\
      --action edit \
      --property plugin.my_plugin \
      --from-file $PLUGIN \
      --target $TARGET \

    # THEN
    assert_failure 6
    assert_output --partial "--from-file « $BATS_TMPDIR/json/my_plugin.json » is not a valid json file"
}

@test "shef-json errors out with 7 if target is missing" {
    # GIVEN
    TARGET=$BATS_TMPDIR/json/config.json
    echo "$(cat <<-EOF
{
  "name": "my_project"
}
EOF
   )" > "$TARGET"

   PLUGIN=$BATS_TMPDIR/json/my_plugin.json
    echo "$(cat <<-EOF
{
  "theme": "molokai"
}
EOF
    )" > "$PLUGIN"

    # WHEN

    run bash ./libexec/shef-json "Title"\
      --action edit \
      --property plugin.my_plugin \
      --from-file $PLUGIN

    # THEN
    assert_failure 7
    assert_output --partial "--target option is missing"
}

@test "shef-json errors out with 8 if --target is not a valid filename" {
    # GIVEN
    TARGET=$BATS_TMPDIR/json/config.json
    echo "$(cat <<-EOF
{
  "name": "my_project"
}
EOF
   )" > "$TARGET"

   PLUGIN=$BATS_TMPDIR/json/my_plugin.json
    echo "$(cat <<-EOF
{
  "theme": "molokai"
}
EOF
    )" > "$PLUGIN"

    # WHEN

    run bash ./libexec/shef-json "Title"\
      --action edit \
      --property plugin.my_plugin \
      --from-file $PLUGIN \
      --target missing-target.json \

    # THEN
    assert_failure 8
    assert_output --partial "--target « missing-target.json » does not exist"
}

@test "shef-json errors out with 9 if --target does not contain valid json" {
    # GIVEN
    TARGET=$BATS_TMPDIR/json/config.json
    echo "$(cat <<-EOF
{
  "name": "my_project
}
EOF
   )" > "$TARGET"

   PLUGIN=$BATS_TMPDIR/json/my_plugin.json
    echo "$(cat <<-EOF
{
  "theme": "molokai"
}
EOF
    )" > "$PLUGIN"

    # WHEN

    run bash ./libexec/shef-json "Title"\
      --action edit \
      --property plugin.my_plugin \
      --from-file $PLUGIN \
      --target $TARGET \

    # THEN
    assert_failure 9
    assert_output --partial "--target « $BATS_TMPDIR/json/config.json » is not a valid json file"
}

@test "shef-json adds a property to existing json file" {
    # GIVEN
    TARGET=$BATS_TMPDIR/json/config.json
    echo "$(cat <<-EOF
{
  "name": "my_project"
}
EOF
   )" > "$TARGET"

   PLUGIN=$BATS_TMPDIR/json/my_plugin.json
    echo "$(cat <<-EOF
{
  "theme": "molokai"
}
EOF
    )" > "$PLUGIN"

    # WHEN

    run bash ./libexec/shef-json "Title"\
      --action edit \
      --property plugin.my_plugin \
      --from-file $PLUGIN \
      --target $TARGET \

    # THEN
    assert_success
    assert_file_is $TARGET "$(cat <<-EOF
{
  "name": "my_project",
  "plugin": {
    "my_plugin": {
      "theme": "molokai"
    }
  }
}
EOF
   )"

}

@test "shef-json removes a property to existing json file if rollback is on" {
    # GIVEN
    TARGET=$BATS_TMPDIR/json/config.json
    echo "$(cat <<-EOF
{
  "name": "my_project",
  "plugin": {
    "my_plugin": {
      "theme": "molokai"
    }
  }
}
EOF
   )" > "$TARGET"

   PLUGIN=$BATS_TMPDIR/json/my_plugin.json
    echo "$(cat <<-EOF
{
  "theme": "molokai"
}
EOF
    )" > "$PLUGIN"

    # WHEN
    run bash ./libexec/shef-json "Title"\
      --action edit \
      --property plugin.my_plugin \
      --from-file $PLUGIN \
      --target $TARGET \
      --rollback

    # THEN
    assert_success
    assert_file_is $TARGET "$(cat <<-EOF
{
  "name": "my_project",
  "plugin": {}
}
EOF
   )"

}
