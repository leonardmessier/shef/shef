load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_SHEF_ROOT/tests/unit/helpers/assert"
load "$_SHEF_ROOT/share/cron/functions"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    mkdir -p "$BATS_TMPDIR/link"
    echo '' > /tmp/shef.log
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi

    rm -rf "$BATS_TMPDIR/link"
    echo '' > /tmp/shef.log
}

@test "shef-link errors out with 5 if --force flag is on and existing symlink could not be removed" {
    # GIVEN
    mkdir -p "$BATS_TMPDIR/link/my-existing-directory"
    ln -s "$BATS_TMPDIR/link/my-existing-directory" "$BATS_TMPDIR/link/my-symlink-to-a-directory"

    shellmock_expect rm \
        --match "-r $BATS_TMPDIR/link/my-symlink-to-a-directory" \
        --status 1

    # WHEN
    run bash ./libexec/shef-link \
        --target "$BATS_TMPDIR/link/my-existing-directory" \
        --to "$BATS_TMPDIR/link/my-symlink-to-a-directory" \
        --force

    # THEN
    assert_failure 5
    assert_output --partial "$(cat <<-EOF
Command « rm -r $BATS_TMPDIR/link/my-symlink-to-a-directory » failed
EOF
    )"
}

@test "shef-link successfully recreates a new symlink on directory if --force flag is on" {
    # GIVEN
    mkdir -p "$BATS_TMPDIR/link/my-existing-directory"
    echo '# My existing directory' >  "$BATS_TMPDIR/link/my-existing-directory/my-file.md"
    ln -s "$BATS_TMPDIR/link/my-existing-directory" "$BATS_TMPDIR/link/my-symlink-to-a-directory"

    export SHEF_QUIET=false
    export SHEF_VERY_VERBOSE=true

    # WHEN
    run bash ./libexec/shef-link \
        --target "$BATS_TMPDIR/link/my-existing-directory" \
        --to "$BATS_TMPDIR/link/my-symlink-to-a-directory" \
        --force \

    # THEN
    assert_success
    assert_output --partial "$(cat <<-EOF
Deleting existing symlink « $BATS_TMPDIR/link/my-symlink-to-a-directory »
EOF
    )"
    assert_file_not_exist "$BATS_TMPDIR/link/my-existing-directory/my-existing-directory"
}

@test "shef-link errors out with 6 if symlink could not be created" {
    # GIVEN
    touch "$BATS_TMPDIR/link/my-existing-file"

    shellmock_expect ln \
        --match "-s $BATS_TMPDIR/link/my-existing-file $BATS_TMPDIR/link/my-symlink" \
        --status 1

    # WHEN
    run bash ./libexec/shef-link \
        --target "$BATS_TMPDIR/link/my-existing-file" \
        --to "$BATS_TMPDIR/link/my-symlink" \

    # THEN
    assert_failure 6
    assert_output --partial "$(cat <<-EOF
Command « ln -s $BATS_TMPDIR/link/my-existing-file $BATS_TMPDIR/link/my-symlink » failed
EOF
    )"

}

@test "shef-link successfully rolls back and removes parent directories" {
    # GIVEN
    touch "$BATS_TMPDIR/link/my-existing-file"
    mkdir -p "$BATS_TMPDIR/link/my/very/complicated/path/to/symlink"
    touch "$BATS_TMPDIR/link/my/very/complicated/test-file"
    ln -s "$BATS_TMPDIR/link/my-existing-file" "$BATS_TMPDIR/link/my/very/complicated/path/to/symlink/my-symlink"

    export SHEF_QUIET=false

    # WHEN
    run bash ./libexec/shef-link "Create symlink" \
        --target "$BATS_TMPDIR/link/my-existing-file" \
        --to "$BATS_TMPDIR/link/my/very/complicated/path/to/symlink/my-symlink" \
        --rollback

    # THEN
    assert_success
    assert_output --partial "$(cat <<-EOF
« Create symlink » operation was rolled back: symlink « $BATS_TMPDIR/link/my/very/complicated/path/to/symlink/my-symlink » was successfully deleted
EOF
    )"
    assert_dir_does_not_exist "$BATS_TMPDIR/link/my/very/complicated/path"
}

@test "shef-link successfully rolls back and but does not remove parent directories if --sudo is on" {
    # GIVEN
    touch "$BATS_TMPDIR/link/my-existing-file"
    mkdir -p "$BATS_TMPDIR/link/my/very/complicated/path/to/symlink"
    touch "$BATS_TMPDIR/link/my/very/complicated/test-file"
    ln -s "$BATS_TMPDIR/link/my-existing-file" "$BATS_TMPDIR/link/my/very/complicated/path/to/symlink/my-symlink"

    export SHEF_QUIET=false

    # WHEN
    run bash ./libexec/shef-link "Create symlink" \
        --target "$BATS_TMPDIR/link/my-existing-file" \
        --to "$BATS_TMPDIR/link/my/very/complicated/path/to/symlink/my-symlink" \
        --sudo \
        --rollback

    # THEN
    assert_success
    assert_output --partial "$(cat <<-EOF
« Create symlink » operation was rolled back: symlink « $BATS_TMPDIR/link/my/very/complicated/path/to/symlink/my-symlink » was successfully deleted
EOF
    )"
    assert_dir_exists "$BATS_TMPDIR/link/my/very/complicated/path"
}
