#!/bin/bash

# shellcheck disable=SC1091
declare -i -r ERROR_QUEUE_PARSE_OPTS=1
declare -i -r ERROR_QUEUE_MISSING_FIRST_ARG=2
declare -i -r ERROR_QUEUE_MISSING_ACTION_ARG=3

shef_queue() {
    source "$_SHEF_ROOT/share/functions"
    source "$_SHEF_ROOT/share/queue/module"
    source "$_SHEF_ROOT/share/resources"

    local NAME=$1

    if ! OPTS=$(getopt -o vhns: --long recipe:,action:,command:,reverse, -n 'parse-options' -- "$@"); then
        shef_error "queue" "Failed parsing options." >&2
        exit $ERROR_QUEUE_PARSE_OPTS
    fi

    eval set -- "$OPTS"

    local RECIPE=
    local ACTION=add
    local COMMAND=
    local REVERSE=false

    while true; do
        case "$1" in
            --recipe ) RECIPE=$2; shift; shift ;;
            --action ) ACTION=$2; shift; shift ;;
            --command ) COMMAND=$2; shift; shift ;;
            --reverse ) REVERSE=true; shift;;
            * ) break;;
        esac
    done

    if [ -z "$NAME" ]; then
        shef_error "queue" "First positionnal argument is mandatory"
        exit $ERROR_QUEUE_MISSING_FIRST_ARG
    fi

    if [ -z "$RECIPE" ]; then
        shef_debug "queue" "Using « ${NAME} » argument as recipe"
        RECIPE=$NAME
    fi

    if [ -z "$ACTION" ]; then
        shef_error "queue" "Argument --action is mandatory"
        exit $ERROR_QUEUE_MISSING_ACTION_ARG
    fi

    if [ "$ACTION" = "add" ] && [ -z "$COMMAND" ]; then
        shef_error "queue" "Argument --command is mandatory if --action is add"
        exit $ERROR_QUEUE_MISSING_ACTION_ARG
    fi

    if [ "$ACTION" = "add" ]; then
        _shef_queue:add "$RECIPE" "$COMMAND"
    elif [ "$ACTION" = "run" ]; then
        if [ "$REVERSE" = "true" ]; then
            _shef_queue:run "$RECIPE" 1
        else
            _shef_queue:run "$RECIPE"
        fi
        return $?
    elif [ "$ACTION" = "reset" ]; then
        _shef_queue:reset "$RECIPE"
    fi
}


if [ "${BASH_SOURCE[0]}" != "$0" ]; then
    export -f shef_queue
else
    shef_queue "${@}"
    exit $?
fi
