#!/usr/bin/env bash
set -e

resolve_link() {
    $(type -p greadlink readlink | head -1) "$1"
}

abs_dirname() {
    local cwd
    cwd="$(pwd)"
    local path="$1"

    while [ -n "$path" ]; do
        cd "${path%/*}"
        local name="${path##*/}"
        path="$(resolve_link "$name" || true)"
    done

    pwd
    cd "$cwd" >> /dev/null 2>&1
}

read_configuration() {
    local CONFIG_FILE

    if [ -z "$XDG_CONFIG_HOME" ]; then
        SHEF_CONFIG="$HOME/.config/shef"
    else
        SHEF_CONFIG="$XDG_CONFIG_HOME/shef"
    fi

    if [ -f "$SHEF_CONFIG/config" ]; then
        CONFIG_FILE="$SHEF_CONFIG/config"
    else
        return 1
    fi

    # shellcheck disable=SC1117
    PARSE=$(jq -r "to_entries|map(\"export SHEF_\(.key|ascii_upcase)='\(.value|tostring)'\")|.[]" "$CONFIG_FILE")
    eval "$PARSE"
}

_SHEF_LIBEXEC_PATH="$(abs_dirname "$0")"
export _SHEF_LIBEXEC_PATH

_SHEF_ROOT="$(abs_dirname "$_SHEF_LIBEXEC_PATH")"
export _SHEF_ROOT

export PATH="${_SHEF_LIBEXEC_PATH}:$PATH"
export SHEF_QUIET=${SHEF_QUIET:-false}
export SHEF_VERBOSE=${SHEF_VERBOSE:-false}
export SHEF_VERY_VERBOSE=${SHEF_VERY_VERBOSE:-false}

# shellcheck disable=SC1091
source "$_SHEF_ROOT/share/functions"

command="$1"
case "$command" in
    "" | "-h" | "--help" )
        exec shef-help
        ;;
    * )
        command_path="$(command -v "shef-$command" || true)"
        if [ ! -x "$command_path" ]; then
            shef_error "shef" "no such command \`$command'" >&2
            exit 1
        fi
        if ! read_configuration; then
            if [ "$command" != "init" ]; then
                shef_error "shef" "No config file was found. This is mandatory to run shef $command"
                exit 1
            fi
        fi

        shift

        exec "$command_path" "$@"
        ;;
esac
