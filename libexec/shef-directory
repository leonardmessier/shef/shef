#!/bin/bash
# Usage: shef directory TITLE|DIRECTORY_PATH OPTIONS
# Summary: create a directory
# Help: Create a directory
#   ARGUMENTS
#     TITLE|DIRECTORY_PATH string to display or directory name
#
#   OPTIONS:
#     --path string the directory path
#     --parents boolean if present, the whole hierarchy will be created default:false
#     --sudo boolean prepend sudo to the create command default:false
#     --rollback boolean Indicates that the block should be run in rollback mode default:false
#     This means removing the directory. If --parents is on and --sudo is not,
#     parent directories will be removed too
#
#   ERROR CODES
#     1 Options could not be parsed
#     2 Missing TITLE|DIRECTORY_PATH argument
#     3 Directory creation failed
#     4 Directory deletion failed
  
shef_directory() {
    # shellcheck disable=SC1091
    source "$_SHEF_ROOT/share/functions"
    # shellcheck disable=SC1091
    source "$_SHEF_ROOT/share/directory/module"
    # shellcheck disable=SC1091
    source "$_SHEF_ROOT/share/options/functions"

    local NAME=$1

    declare -A OPTS
    options:parse "directory" OPTS "$@"

    if [ -z "$NAME" ]; then
        shef_error "directory" "First positionnal argument is mandatory"
        # shellcheck disable=SC2086
        return $ERROR_DIRECTORY_MISSING_FIRST_ARG
    fi

    if [ -z "${OPTS[path]}" ]; then
        shef_debug "directory" "Using « ${NAME} » argument as path"
        OPTS[path]=$NAME
    fi

    if [ "${OPTS[rollback]}" = "false" ]; then
        _shef_directory_rollout "$NAME" "${OPTS[path]}" "${OPTS[parents]}" "${OPTS[sudo]}"
    else
        _shef_directory_rollback "$NAME" "${OPTS[path]}" "${OPTS[parents]}" "${OPTS[sudo]}"
    fi

    return $?
}

if [ "${BASH_SOURCE[0]}" != "$0" ]; then
    export -f shef_directory
else
    shef_directory "${@}"
    exit $?
fi
