#!/bin/bash

# shellcheck disable=SC1091
source "$_SHEF_ROOT/share/functions"

NAME=$1

if ! OPTS=$(getopt -o vhns: --long recipe: -n 'parse-options' -- "$@"); then
    shef_error "update-recipe" "Failed parsing options." >&2
    exit 1
fi

eval set -- "$OPTS"

RECIPE=

while true; do
    case "$1" in
        --recipe ) RECIPE=$2; shift; shift ;;
        * ) break;;
    esac
done

if [ -z "$NAME" ]; then
    shef_error "update-recipe" "First positionnal argument is mandatory"
    exit 2
fi

if [ -z "$RECIPE" ]; then
    shef_debug "update-recipe" "Using « ${NAME} » argument as recipe"
    RECIPE=$NAME
fi

RECIPE_NAME=$(echo "$RECIPE" | cut -d/ -f 2)
if [ "$SHEF_GIT_RECIPE_PROTOCOL" == 'https' ]; then
  REPO_URL="https://${SHEF_GIT_REMOTE}/${SHEF_GIT_REMOTE_GROUP}/${RECIPE_NAME}.git"
elif [ "$SHEF_GIT_RECIPE_PROTOCOL" == 'http' ]; then
  REPO_URL="http://${SHEF_GIT_REMOTE}/${SHEF_GIT_REMOTE_GROUP}/${RECIPE_NAME}.git"
elif [ "$SHEF_GIT_RECIPE_PROTOCOL" == 'git' ]; then
  REPO_URL="git@${SHEF_GIT_REMOTE}:${SHEF_GIT_REMOTE_GROUP}/${RECIPE_NAME}.git"
else
  shef_error "download-recipe" "Configuration value « $SHEF_GIT_RECIPE_PROTOCOL » for git_recipe_protocol is invalid"
  exit 3
fi

shef git "${REPO_URL}" \
    --destination "${SHEF_RECIPES_DRAWER}/${RECIPE}" \
