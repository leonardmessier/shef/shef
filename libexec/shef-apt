#!/bin/bash
# Usage: shef apt TITLE|PACKAGE [options]
# Summary: Install or remove one or several apt packages
# Help: Use apt to install or remove one or several apt packages.
#
#   ARGUMENTS:
#     TITLE|PACKAGE string to display while invoking the resource or package
#     name if --package is missing
#
#   OPTIONS:
#     --package string List of packages to install|remove, comma separated
#     --action string The action to perform. Either remove or install default:install
#     --repository string URI of the specific debian repository to use
#     --rollback boolean Indicates that the block should be run in rollback mode default:false
#
#   ERROR CODES:
#     1 Options could not be parsed
#     2 Missing TITLE|PACKAGE argument
#     3 Apt command failed
#
#   EXAMPLES:
#     1. Install rofi package in shell script
#     $ shef apt rofi
#
#     2. Install docker in .recipe file
#     apt "Install right docker version"; do
#       package docker-ce
#       repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $SHEF_UBUNTU_RELEASE stable"
#     done
  
shef_apt() {
    # shellcheck disable=SC1091
    source "$_SHEF_ROOT/share/functions"
    # shellcheck disable=SC1091
    source "$_SHEF_ROOT/share/apt/module"
    # shellcheck disable=SC1091
    source "$_SHEF_ROOT/share/options/functions"

    local NAME=$1

    declare -A OPTS
    options:parse "apt" OPTS "$@"
    if [ -n "${OPTS[packages]}" ]; then
        OPTS[package]="${OPTS[packages]}"
        # shellcheck disable=SC2184,SC2102
        unset OPTS[packages]
        shef_warning "apt" "--packages option is deprecated and will be removed in future release. Use --package instead"
    fi

    if [ "${OPTS[rollback]}" == "true" ]; then
        _shef_apt_rollback "$NAME" "${OPTS[package]}" "${OPTS[action]}" "${OPTS[repository]}"
        return $?
    else
        _shef_apt_rollout "$NAME" "${OPTS[package]}" "${OPTS[action]}" "${OPTS[repository]}"
        return $?
    fi
}

if [ "${BASH_SOURCE[0]}" != "$0" ]; then
    export -f shef_apt
else
    shef_apt "${@}"
    exit $?
fi
