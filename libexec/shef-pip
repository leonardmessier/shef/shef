#!/bin/bash

# Usage: shef pip TITLE|PACKAGE [options]
# Summary: Install a pip package
# Help: Install a pip package
#
#   ARGUMENTS:
#     TITLE|PACKAGE string to display while invoking the resource or package
#     name if --package is missing
#
#   OPTIONS:
#     --package string Package to install
#     --rollback boolean Indicates that the block should be run in rollback mode default:false
#
#   ERROR CODES:
#     1 Options could not be parsed
#     2 Missing TITLE|PACKAGE argument
#     3 Pip package installation failed
#     4 Pip package deinstallation failed

shef_pip() {
    # shellcheck disable=SC1091
    source "$_SHEF_ROOT/share/functions"
    # shellcheck disable=SC1091
    source "$_SHEF_ROOT/share/pip/module"
    # shellcheck disable=SC1091
    source "$_SHEF_ROOT/share/options/functions"

    NAME=$1

    declare -A OPTS
    options:parse "pip" OPTS "$@"

    if [ -z "$NAME" ]; then
        shef_error "pip" "First positionnal argument is mandatory"
        # shellcheck disable=SC2086
        return $ERROR_PIP_MISSING_FIRST_ARG
    fi

    if [ -z "${OPTS[package]}" ]; then
        shef_info "pip" "Using « ${NAME} » argument as package"
        OPTS[package]=$NAME
    fi

    if [ "${OPTS[rollback]}" == "false" ]; then
        _shef_pip_rollout "${OPTS[package]}"
    else
        _shef_pip_rollback "${OPTS[package]}"
    fi

    return $?
}

if [ "${BASH_SOURCE[0]}" != "$0" ]; then
    export -f shef_pip
else
    shef_pip "${@}"
    exit $?
fi
