#!/bin/bash
# Usage: shef build RECIPE_NAME INPUT
# Summary: transpiles a recipe file into a shell script
# Help: Transpiles a recipe file into a shell script
#
#   ARGUMENTS:
#     RECIPE_NAME the short name of the recipe
#     INPUT  Path to the recipe file to build
#
#   ERROR CODES:
#     1 Missing recipe name
#     2 Missing input file argument
#     3 Unexisting input file
#     4 Build error
#
#   EXAMPLES:
#   $ shef build jrnl install.recipe
  
# shellcheck disable=SC1091
source "$_SHEF_ROOT/share/functions"
source "$_SHEF_ROOT/share/awk/functions"

shef_build() {
    local RECIPE_NAME=
    local INPUT=

    if [ -z "$1" ]; then
        shef_error "build" "Missing recipe name"
        return 1
    fi

    if [ -z "$2" ]; then
        shef_error "build" "Missing input file"
        return 2
    fi

    if [ ! -f "$2" ]; then
        shef_error "build" "Input file « $1 » does not exist"
        return 3
    fi

    RECIPE_NAME=$1
    INPUT=$2

    local COMPILED
    COMPILED="$(_shef_build_command "$RECIPE_NAME" "$INPUT")"
    # shellcheck disable=SC2181
    if [ "$?" -ne 0 ] ; then
        shef_error "build" "Recipe file « $INPUT » could not be compiled"
        return 4
    fi

    echo -e "$COMPILED"
}

if [ "${BASH_SOURCE[0]}" != "$0" ]; then
    export -f shef_build
else
    shef_build "${@}"
    exit $?
fi
