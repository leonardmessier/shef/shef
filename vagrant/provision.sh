#!/bin/bash

set -e

sudo apt update --allow-releaseinfo-change
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -

sudo apt-get install -y \
  git \
  build-essential \
  xterm \
  curl \
  file \
  git \
  python3 \
  python3-pip \
  software-properties-common \
  nodejs \

sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
test -d ~/.linuxbrew && eval "$(~/.linuxbrew/bin/brew shellenv)"
test -d /home/linuxbrew/.linuxbrew && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile

sudo mkdir -p /home/linuxbrew/.linuxbrew/var/homebrew/linked
sudo chown -R "$(whoami)" /home/linuxbrew/.linuxbrew/var/homebrew/linked&
brew doctor
brew install shml wget git go jq

SHEF_CACHE_DIR="$HOME/.cache/shef"
if [ ! -d "$SHEF_CACHE_DIR" ]; then
  mkdir -p "$SHEF_CACHE_DIR"
fi

if [ "$ENVIRONMENT" != "dev" ]; then
  if [ ! -d "$SHEF_CACHE_DIR/src" ]; then
    git clone https://gitlab.com/leonardmessier/shef/shef.git "$SHEF_CACHE_DIR/src"
  else
    cd "$SHEF_CACHE_DIR/src" || exit 1
    git pull
    cd - || exit 1
  fi
fi

mkdir -p ~/bin
sudo ln -f -s "$SHEF_CACHE_DIR/src/bin/shef" /usr/local/bin/shef

mkdir -p "$HOME/.config/autostart"
ln -s "$SHEF_CACHE_DIR/src/vagrant/autostart/setkeyboard.desktop" "$HOME/.config/autostart/setkeyboard.desktop"
sudo ln -s "$SHEF_CACHE_DIR/src/vagrant/setkeyboard" /usr/local/bin/setkeyboard

sudo sed --in-place s/us/fr/g /etc/default/keyboard
echo "autologin-enable=true" | sudo tee --append /etc/lightdm/lightdm.conf.d/70-linuxmint.conf
echo "autologin-user=vagrant" | sudo tee --append /etc/lightdm/lightdm.conf.d/70-linuxmint.conf

shef init
